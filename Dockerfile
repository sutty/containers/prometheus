FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache prometheus daemonize
COPY ./monit.conf /etc/monit.d/prometheus.conf
COPY ./prometheusd.sh /usr/local/bin/prometheusd

EXPOSE 9090
VOLUME /var/lib/prometheus
