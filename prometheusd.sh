#!/bin/sh

pid=/tmp/prometheus.pid
var=/var/lib/prometheus

case $1 in
  stop)
    test -f "${pid}" || exit 0

    cat "${pid}" | xargs -r kill

    rm "${pid}"
    ;;
  start)
    rm -f "${pid}"

    chown prometheus:prometheus "${var}"
    chmod -R u=rwX,g=rX,o= "${var}"

    daemonize -p "${pid}" -l "${pid}" -u prometheus -c "${var}" \
      /usr/bin/prometheus --config.file=/etc/prometheus/prometheus.yml --storage.tsdb.path="${var}/data"
    ;;
esac

exit $?
